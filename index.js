// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

//This allows us to use all the routes defined in the "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Set up server
const app = express();
const port = 3001;

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//MongoDB connection link
mongoose.connect("mongodb+srv://admin:admin@course-booking.go6r6.mongodb.net/b183_to-do?retryWrites=true&w=majority", {useNewURLParser: true, useUnifiedTopology: true});

// Set the notification for connection or failure

let db = mongoose.connection;

//Notify on error
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

//Add the task route
//Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
//localhost:3001/tasks
app.use("/tasks", taskRoute);

//Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));

/*
	Separation of concerns

	Model Folder
	- Contains the object Schemas and defines the object structure and content.

	Controllers
	- Contain the function and business logic of our JS application.
	- Meaning all the operations it can do will be placed here.

	Routes
	- Contain all the endpoints and assign the http methods for our application. app.get("/", )
	- We separate the routes such thaat the server/"index.js" only contains information on the server.

	JS modules
	require => to include a specific module/package.
	export.module => to treat a value as a "package" that can be used by other files.

	Flow of exports and require:
	export models > require in controllers
	export controllers > require in routes
	export routes > require in server (index.js)

*/